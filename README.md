# Unset HTML head link (Drupal 8.x)

If you want SEO boosting with Drupal 8.x site, we recomended to delete this HTML head links:

```html
<link rel="delete-form" href="/node/1/delete" />
<link rel="edit-form" href="/node/1/edit" />
<link rel="version-history" href="/node/1/revisions" />
<link rel="revision" href="/page-1-name" />
```
This module ``Unset HTML head link`` can do it. Fast and simply!

Support:

* Node
* Taxonomy term

## Install

* composer require wkse/unset_html_head_link
* Enable this module at ``/admin/modules`` (look in ``Search engine optimization`` pack).
* _Don't forget to flush cache!_
